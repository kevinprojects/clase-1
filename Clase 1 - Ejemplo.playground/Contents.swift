//: Playground - noun: a place where people can play
// Dejar un comentario.

import UIKit

var nombre = "Kevin"
var unNumero: Float = 4.0
let pi = 3.14

unNumero = 3.4


if unNumero > 7 {
    
} else {
    // Aca entra si es menor a 7.
}

let arrayNumeros = [1,3,5,6]

for numero in arrayNumeros {
    // numero -> 1
    // numero -> 3
    // numero -> 5
    // numero -> 6
}

switch nombre {
    case "Kevin": break
    case "Martin":
    print(nombre)
    case "etc":
    print(nombre)
    default:
    print(nombre)
}

class Persona {
    var nombre: String
    private var edad: Int
    
    init() {
        nombre = ""
        edad = 0
    }
    
    init(unNombre nombreParaInicializar: String) {
        nombre = nombreParaInicializar
        edad = 0
    }

    init(unNombre: String, laEdad: Int) {
        nombre = unNombre
        edad = laEdad
    }
}


let personaConcretoKevin = Persona()
personaConcretoKevin.nombre = "Jordan"


struct Moto {
    var color: String
    var cantCeniceros: Int
    
    init() {
        color = ""
        cantCeniceros = 4
    }
}

class Auto {
    var cantPuertas: Int
    init() {
        cantPuertas = 0
    }
}

// Ejemplo de referencia:

let autoKevin = Auto()
autoKevin.cantPuertas = 2
print(autoKevin.cantPuertas)

var autoRobado = autoKevin
autoRobado.cantPuertas = 1

print(autoKevin.cantPuertas)
print(autoRobado.cantPuertas)

autoKevin.cantPuertas = 2

print(autoRobado.cantPuertas)

// ---

// Ejemplo de copia:
var motoRulo = Moto()
motoRulo.cantCeniceros = 4
print(motoRulo.cantCeniceros)

var motoJaime = motoRulo
print(motoJaime.cantCeniceros)

motoRulo.cantCeniceros = 2
print(motoRulo.cantCeniceros)
print(motoJaime.cantCeniceros)

enum TipoDeCodigos {
    case QR(String)
    case CodigoBarra(Int)
}

struct Producto {
    var codigo: TipoDeCodigos
}

let mayonesa = Producto(codigo: .CodigoBarra(1234))
let computadora = Producto(codigo: TipoDeCodigos.QR("http://google.com"))

let arrayProductos = [mayonesa, computadora]

for producto in arrayProductos {
    switch producto.codigo {
        case TipoDeCodigos.QR(let valorDelQR):
            print(valorDelQR)
            //Abrir pagina web.
        case TipoDeCodigos.CodigoBarra(let valorDelCodigoDeBarra):
            print(valorDelCodigoDeBarra)
            //Imprimir el codigo.
    }
}


var jaime: String = "Jaimito"
var pepe: String = ""


